public class Teacher extends Person implements AttendClasses {
  
  private String subject;
  private Integer salary;
  
  public Teacher(String name, Integer age, String subject, Integer salary) {
    super(name, age);
    this.subject = subject;
    this.salary = salary;
  }
  
  public String getSubject() {
    return subject;
  }
  
  public void setSubject(String subject) {
    this.subject = subject;
  }
  
  public Integer getSalary() {
    return salary;
  }
  
  public void setSalary(Integer salary) {
    this.salary = salary;
  }
  
  public Integer salary(Integer bonus) {
    return this.salary + bonus;
  }
  
  public Integer salary(Integer bonus, Integer rate) {
    return (this.salary + bonus) * rate;
  }
  
  @Override
  public Boolean isAttended(Boolean isInterested) {
    return isInterested;
  }
  
}
