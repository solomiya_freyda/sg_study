public class Student extends Person implements AttendClasses {
  
  private String faculty;
  private Double average;
  
  public Student(String name, Integer age, String faculty, Double average) {
    super(name, age);
    this.faculty = faculty;
    this.average = average;
  }
  
  public String getFaculty() {
    return faculty;
  }
  
  public void setFaculty(String faculty) {
    this.faculty = faculty;
  }
  
  public Double getAverage() {
    return average;
  }
  
  public void setAverage(Double average) {
    this.average = average;
  }
  
  @Override
  public Boolean isAttended(Boolean isInterested) {
    return isInterested;
  }
  
}
