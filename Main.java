import java.util.ArrayList;
import java.util.List;

public class Main {
  
  public static void main(String[] args) {
  
    Person firstStudent = new Student("Phil Collins", 19, "Hedgehogs", 23.5);
    Person secondStudent = new Student("Zac Efron", 21, "Progressives", 25.0);
  
    Person firstTeacher = new Teacher("Chris Evans", 45, "Rush", 15000);
    Person secondTeacher = new Teacher("Jason Momoa", 51, "Classic Heavy", 21000);
  
    System.out.println("Mr " + firstTeacher.getName() + " has a salary plus bonus of: " + ((Teacher) firstTeacher).salary(2500) + "\n");
    System.out.println("Mr " + secondTeacher.getName() + " has a salary plus bonus and rates of: " + ((Teacher) secondTeacher).salary(1500,10) + "\n");
    
    List<Person> persons = new ArrayList<>();
    persons.add(firstStudent);
    persons.add(secondStudent);
    persons.add(firstTeacher);
    persons.add(secondTeacher);

    persons.forEach(person -> {
      searchPersonByName("Phil Collins", person);
      if (person.getClass().equals(Teacher.class)) {
        searchTeacherBySubject("Classic Heavy", (Teacher) person);
      }
      if (person.getClass().equals(Student.class)) {
        searchStudentByFaculty("Progressives", (Student) person);
      }
    });
  
    System.out.println("Person " + firstStudent.getName() + " attend status is: " + ((Student) firstStudent).isAttended(true) + "\n");
    System.out.println("Person " + secondTeacher.getName() + " attend status is: " + ((Teacher) secondTeacher).isAttended(false) + "\n");
    
  }
  
  public static void searchStudentByFaculty(String faculty, Student student) {
    if (student.getFaculty().equals(faculty)) {
      System.out.println("The student belonging in faculty " + faculty + " is " + student.getName() + "\n");
    }
  }
  
  public static void searchPersonByName(String name, Person person) {
    if (person.getName().equals(name)) {
      System.out.println("Found person with name " + person.getName() + "\n");
    }
  }
  
  public static void searchTeacherBySubject(String subject, Teacher teacher) {
    if (teacher.getSubject().equals(subject)) {
      System.out.println("The teacher with subject " + subject + " is " + teacher.getName() + "\n");
    }
  }
  
}
